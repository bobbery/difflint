#ifndef WORKERTHREAD_H
#define WORKERTHREAD_H

#include <QObject>
#include <QProcess>
#include <QThread>

class WorkerThread : public QThread
{
    Q_OBJECT
public:
    WorkerThread(const QString & sourceFile, const QString & tcPath);

    virtual void run();

    ~WorkerThread();

signals:

    void processFinished(const QString &);



private:
    QProcess *m_process;
    QString m_sourceFile;
    QString m_tcPath;
};

#endif // WORKERTHREAD_H
