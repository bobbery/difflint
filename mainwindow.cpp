#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QFileDialog>
#include <QDebug>
#include <QSettings>
#include <QDirIterator>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    connect(ui->btnBrowseSource, SIGNAL(clicked()), this, SLOT(browseSource()));
    connect(ui->btnBrowseToolchain, SIGNAL(clicked()), this, SLOT(browseToolchain()));
    connect(ui->btnCheckForModifications, SIGNAL(clicked()), this, SLOT(findModifications()));
    connect(ui->btnLint, SIGNAL(clicked()), this, SLOT(runAllLint()));

    QSettings settings;
    ui->leSource->setText(settings.value("lastSource").toString());
    ui->leToolchain->setText(settings.value("lastToolchain").toString());

    m_process = new QProcess(this);

    ui->btnLint->setEnabled(false);

    ui->progressBar->setValue(0);

    m_processedItems = 0;
    m_tasksRunning = 0;
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::browseSource()
{
    QString sourceFolder = QFileDialog::getExistingDirectory(this, "Select source folder ...");
    ui->leSource->setText(sourceFolder);

    if (!sourceFolder.isEmpty())
    {
        QSettings settings;
        settings.setValue("lastSource", sourceFolder);
    }
}

void MainWindow::browseToolchain()
{
    QString toolchainFolder = QFileDialog::getExistingDirectory(this, "Select toolchain folder ...");
    ui->leToolchain->setText(toolchainFolder);

    if (!toolchainFolder.isEmpty())
    {
        QSettings settings;
        settings.setValue("lastToolchain", toolchainFolder);
    }
}

void MainWindow::findModifications()
{
    ui->lwFiles->clear();
    connect(m_process, SIGNAL(finished(int)), this, SLOT(svnStatusFinished()));

    QString program = "svn";
    QStringList arguments;
    arguments << "status";

    m_process->setWorkingDirectory(ui->leSource->text());

    m_process->start(program, arguments);
    ui->btnCheckForModifications->setEnabled(false);
}

void MainWindow::svnStatusFinished()
{
    QString data = m_process->readAll();

    QStringList list = data.split('\n');

    for (const QString& line : list)
    {
        if (line.startsWith('M') || line.startsWith('A'))
        {
            if (line.simplified().endsWith(".h") || line.simplified().endsWith(".cpp"))
            {
                ui->lwFiles->addItem(line);
            }
        }
    }

    ui->btnCheckForModifications->setEnabled(true);

    disconnect(m_process, SIGNAL(finished(int)), this, SLOT(svnStatusFinished()));

    ui->btnLint->setEnabled(true);
}





void MainWindow::runAllLint()
{
    ui->btnLint->setEnabled(false);
    ui->btnCheckForModifications->setEnabled(false);

    ui->outputWindow->clear();
    m_currentItem = 0;

    while (m_currentItem < ui->lwFiles->count())
    {
        lintNextFile();
        m_currentItem++;
    }

    qDebug() << "Starting threads ...";

    while (m_tasksRunning < 8 && !m_tasks.empty())
    {
        WorkerThread *thread = m_tasks.front();
        m_tasks.pop_front();
        m_tasksRunning++;
        thread->start();
    }
}

void MainWindow::lintFinished(QString const& output)
{
    qDebug() << "Lint finished !" << output;

    ui->outputWindow->append(output);

    m_processedItems++;

    int percentage = m_processedItems * 100 / ui->lwFiles->count();
    ui->progressBar->setValue(percentage);

    m_tasksRunning--;

    if (m_processedItems == ui->lwFiles->count())
    {
        ui->btnLint->setEnabled(true);
        ui->btnCheckForModifications->setEnabled(true);
    }

    qDebug() << "Starting threads ...";

    while (m_tasksRunning < 8 && !m_tasks.empty())
    {
        WorkerThread *thread = m_tasks.front();
        m_tasks.pop_front();
        m_tasksRunning++;
        thread->start();
    }
}

void MainWindow::lintNextFile()
{
    QString path = ui->leSource->text();
    QString svnName = ui->lwFiles->item(m_currentItem)->text();

    svnName = svnName.split(" ").back();
    svnName = svnName.simplified();
    QString sourceFile = path + QDir::separator() + svnName;

    m_tasks.push_back(new WorkerThread(sourceFile, ui->leToolchain->text()));
    connect(m_tasks.back(), SIGNAL(processFinished(const QString &)), this, SLOT(lintFinished(const QString &)));
}
