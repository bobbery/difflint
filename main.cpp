#include "mainwindow.h"
#include <QApplication>

int main(int argc, char *argv[])
{
    QCoreApplication::setOrganizationName("Elektobit");
    QCoreApplication::setOrganizationDomain("elektobit.com");
    QCoreApplication::setApplicationName("DiffLint");

    QApplication a(argc, argv);
    MainWindow w;
    w.show();

    return a.exec();
}
