#include "workerthread.h"

#include <QDir>
#include <QDebug>
#include <QDirIterator>


WorkerThread::WorkerThread(const QString &sourceFile, const QString & tcPath)
    : m_sourceFile(sourceFile)
    , m_tcPath(tcPath)
{

}

QString findProjectFolder(QString const & sourceFile)
{
    QDir dir(sourceFile);

    while (!dir.entryList().contains("CMakeLists.txt"))
    {
        if(!dir.cdUp())
        {
            break;
        }
    }

    return dir.absolutePath();
}

QString getProjectName(QString const & projectPath)
{
    QFile f(projectPath + QDir::separator() + "CMakeLists.txt");
    f.open(QIODevice::ReadOnly);
    QString pn;

    while (!f.atEnd())
    {
        QString line = f.readLine().simplified();
        if (line.startsWith("add_executable") || line.startsWith("add_library") || line.startsWith("project"))
        {
            qDebug() << line;
            QString tmp = line.split("(").at(1);
            tmp = tmp.split(")").at(0);
            pn = tmp.split(" ").at(0);
            break;
        }
    }

    return pn;
}

QString findToolchainFolder(QString const &directory, QString const & projectName)
{
    QDirIterator it(directory, QStringList() << projectName + ".vcproj", QDir::Files, QDirIterator::Subdirectories);
    if (it.hasNext())
    {
        QFileInfo info(it.next());
        return info.absolutePath();
    }

    return QString();
}

void WorkerThread::run()
{
    QString projectPath = findProjectFolder(m_sourceFile);
    QString projectName = getProjectName(projectPath);

    qDebug() << "Project Name: " << projectName;

    QString toolchainFolder = findToolchainFolder(m_tcPath, projectName);

    if (toolchainFolder.isEmpty())
    {
        projectName += "_s";
        toolchainFolder = findToolchainFolder(m_tcPath, projectName);
    }

    qDebug() << toolchainFolder;

    QString program = "C:\\lint\\lint-nt.exe";
    QStringList arguments;
    arguments << "-i\"C:\\Lint\"";
    arguments << "\"c:\\lint\\configs\\MCO\\std_w2.lnt\"";
    arguments << "--u" <<  QString("\"") + toolchainFolder + QDir::separator() + projectName + ".lnt_includes.tmp.lnt\"";
    arguments << QString("\"") + toolchainFolder + QDir::separator() + projectName + ".lnt.tmp.lnt\"";
    arguments << QString("\"") + m_sourceFile + "\"";

    m_process = new QProcess();
    m_process->setWorkingDirectory(projectPath);
    m_process->start(program, arguments);
    m_process->waitForFinished(-1);
    emit processFinished(m_process->readAll());
    delete m_process;
}

WorkerThread::~WorkerThread()
{

}

