#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "workerthread.h"

#include <QMainWindow>
#include <QProcess>
#include <QQueue>


namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void browseSource();
    void browseToolchain();
    void findModifications();
    void svnStatusFinished();
    void runAllLint();
    void lintFinished(QString const& output);
    void lintNextFile();

private:
    Ui::MainWindow *ui;
    QProcess *m_process;
    QQueue<WorkerThread *> m_tasks;
    int m_currentItem;
    int m_processedItems;

    int m_tasksRunning;
};

#endif // MAINWINDOW_H
